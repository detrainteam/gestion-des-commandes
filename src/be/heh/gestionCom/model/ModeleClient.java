/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package be.heh.gestionCom.model;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Locale;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Logan et Kevin Detrain
 */
public class ModeleClient extends AbstractTableModel
{
    private ArrayList<Client> clients = null;
    private String[] entetes = null;

    public ModeleClient()
    {
        clients = Client.getClients();
        entetes = new String[]{"Identifiant", "Nom", "Prenom", "Date de création","Email","Mot de passe","Adresse", "Code postal", "Ville"};
    }

    @Override
    public String getColumnName(int columnIndex)
    {
        if(entetes==null)
            return "";

        return entetes[columnIndex];
    }

    @Override
    public int getRowCount() {
        return (clients!=null) ? clients.size() : 0;
    }

    @Override
    public int getColumnCount() {
        return (entetes!=null) ? entetes.length : 0;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        // Les clients n'ont pas peu être récupèrés. Pour éviter un plantage avec clients.get(), on renvoye directement null.
        if(clients==null)
            return null;

        // rowIndex n'est pas correcte. Pour éviter un plantage avec clients.get(), on renvoye directement null.
        if(rowIndex<0 || rowIndex>clients.size())
            return null;

        Client client = clients.get(rowIndex);
        Object obj = null;

        switch(columnIndex)
        {
            case 0: //Identifiant
                obj = client.getIdentifiant();
                break;
            case 1: //Nom
                obj = client.getNom();
                break;
            case 2: //Prenom
                obj = client.getPrenom();
                break;
            case 3: //Date de création
                Locale locale = Locale.getDefault();
                DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, locale);

                obj = (client.getDateCreation()!= null) ? dateFormat.format(client.getDateCreation()) : null;
                break;
            case 4: //Adresse email
                obj = client.getAdresseEmail();
                break;
            case 5: //Mot de passe
                obj = client.getMdp();
                break;
            case 6: //Adresse
                obj = client.getAdresse();
                break;
            case 7: //Code postal
                obj = client.getCodePostal();
                break;
            case 8: //Ville
                obj = client.getVille();
                break;
        }

        return obj;
    }
}
