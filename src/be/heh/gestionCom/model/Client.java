/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package be.heh.gestionCom.model;

import be.heh.gestionCom.controle.connexion.ConnexionDB;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Logan et Kevin Detrain
 */
public class Client
{
    // Tableau dynamique qui contiendra l'ensemble des clients présent dans la base de données.
    private static ArrayList<Client> clients = null;

    // Valeur qui doivent obligatoirement être renseignées
    private String identifiant = null;
    private String nom = null;
    private String prenom = null;
    private Date dateCreation = null;
    private String email = null;
    private String mdp = null;
    private String adresse = null;
    private int codePostal = 0;
    private String ville = null;

    // Constructeur
    public Client(String identifiant, String nom, String prenom, Date dateCreation, String email, String mdp, String adresse, int codePostal, String ville)
    {
        this.identifiant = identifiant;
        this.nom = nom;
        this.prenom = prenom;
        this.dateCreation = dateCreation;
        this.email = email;
        this.mdp = mdp;
        this.adresse = adresse;
        this.codePostal = (codePostal>0) ? codePostal : 0;
        this.ville = ville;
    }

    /*
        Cette Fonction créer un utilisateur dans la base de donnée.
        Elle retourne true si l'utilisateur à bien été créé et false dans le cas contraire.
    */
    public static boolean creerClient(String id, String nom, String prenom, String email, String mdp, String adresse, int codePostal, String ville)
    {
        Connection con = ConnexionDB.getConnexion();
        PreparedStatement stat = null;

        /*  La date est la seule information qui n'est pas founie par l'utilisateur de la methode creerClient()
            afin qu'il ne puisse pas antidater la création du client. */
        java.util.Date date = new java.util.Date();
        java.sql.Date dateCreation = new java.sql.Date(date.getTime());
        
        try
        {
            stat = con.prepareStatement("INSERT INTO  clients(id, nom, prenom, mot_de_passe, email, date_creation, adresse, code_postal, ville) VALUES (?,?,?,?,?,?,?,?,?)");
            stat.setObject(1, id, Types.VARCHAR);
            stat.setObject(2, nom, Types.VARCHAR);
            stat.setObject(3, prenom, Types.VARCHAR);
            stat.setString(4, mdp);
            stat.setObject(5, email, Types.VARCHAR);
            stat.setDate(6, dateCreation);
            stat.setObject(7, adresse, Types.VARCHAR);
            stat.setInt(8, codePostal);
            stat.setObject(9, ville, Types.VARCHAR);
            stat.execute();

            // S'il n'y à pas eu d'erreur jusqu'ici, c'est que tout c'est bien passé.
            return true;
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Erreur " + ex);
        }

        return false;
    }

    /*
        Cette Fonction modifie un utilisateur de la base de donnée.
        Elle retourne true si l'utilisateur à bien été créé et false dans le cas contraire.
    */    
    public static boolean modifierClient(String id, String nouveauId, String nom, String prenom, String mdp, String email, String adresse, int codePostal, String ville)
    {
	Connection con = ConnexionDB.getConnexion();
        PreparedStatement stat = null;
        
        try
        {
            stat = con.prepareStatement("UPDATE clients SET id=?,nom=?,prenom=?,mot_de_passe=?,email=?,adresse=?,code_postal=?,ville=? WHERE id=?");
            stat.setObject(1, nouveauId, Types.VARCHAR);
            stat.setObject(2, nom, Types.VARCHAR);
            stat.setObject(3, prenom, Types.VARCHAR);
            stat.setString(4, mdp);
            stat.setObject(5, email, Types.VARCHAR);
            stat.setObject(6, adresse, Types.VARCHAR);
            stat.setObject(7, codePostal, Types.INTEGER);
            stat.setObject(8, ville, Types.VARCHAR);
            stat.setObject(9, id, Types.VARCHAR);
            stat.execute();

            return true;
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Erreur " + ex);
        }

        return false;
    }

    /*
        Cette Fonction supprime un utilisateur de la base de donnée.
        Elle retourne true si l'utilisateur à bien été créé et false dans le cas contraire.
    */
    public static boolean supprimerClient(String id)
    {
        Connection con = ConnexionDB.getConnexion();
        PreparedStatement stat = null;
        
        try
        {
            stat = con.prepareStatement("DELETE FROM clients WHERE id=?");
            stat.setObject(1, id, Types.VARCHAR);
            stat.execute();

            return true;
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Erreur " + ex);
        }

        return false;
    }

    /*
        Cette Fonction créer un ArrayList contenant l'ensemble des utilisateurs et stocke sa référence dans "clients".
    */
    public static void lectureSaveTabCRUD()
    {
        Connection con = ConnexionDB.getConnexion();
        clients = new ArrayList<>();

        try
        {
            Statement stat = con.createStatement();
            ResultSet resultat = stat.executeQuery("SELECT * FROM clients");

            while(resultat.next())
            {
                String tempIdentifiant = resultat.getString("id");
                String tempNom = resultat.getString("nom");
                String tempPrenom = resultat.getString("prenom");
                Date tempDateCreation = resultat.getDate("date_creation");
                String tempEmail = resultat.getString("email");
                String tempMdp = resultat.getString("mot_de_passe");
                String tempAdresse = resultat.getString("adresse");
                int tempCodePostal = resultat.getInt("code_postal");
                String tempVille = resultat.getString("ville");

                clients.add(new Client(tempIdentifiant, tempNom, tempPrenom, tempDateCreation, tempEmail, tempMdp, tempAdresse, tempCodePostal, tempVille));
            }
        }
        catch (SQLException ex)
        {
            JOptionPane.showMessageDialog(null, "Erreur " + ex + "\nLa liste des clients n'a pas peu être mise-à-jours.");
        }
    }

    public static ArrayList<Client> getClients()
    {
        //On s'assure de toujours retourner une liste à jours
        lectureSaveTabCRUD();
        return clients;
    }

    public String getIdentifiant()
    {
        return identifiant;
    }

    public String getNom()
    {
        return nom;
    }

    public String getPrenom()
    {
        return prenom;
    }

    public Date getDateCreation()
    {
        return dateCreation;
    }

    public String getAdresseEmail()
    {
        return email;
    }

    public String getMdp()
    {
        return mdp;
    }

    public String getAdresse()
    {
        return adresse;
    }

    public int getCodePostal()
    {
        return codePostal;
    }

    public String getVille()
    {
        return ville;
    }
}
