package be.heh.gestionCom.controle.connexion;
     
import java.sql.Connection;
import javax.swing.JOptionPane;
import be.heh.gestionCom.entite.Parametres;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//classe : pattern singleton
public class ConnexionDB {

    private static Connection con = null;

    private ConnexionDB() {
        
    }

    public static Connection getConnexion() {
        if (ConnexionDB.con == null) {
            return initialiserConnexion();
        } else {
            return ConnexionDB.con;
        }
    }

    private static Connection initialiserConnexion() {
        boolean estCharge = true;
        Parametres lesParams = new Parametres();
        //1. Chargement du driver
        //-----------------------
        try {
            
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Classe non trouvée "
                    + "pour le chargement du pilote JDBC MySQL", "ALERTE",
                    JOptionPane.ERROR_MESSAGE);
            estCharge = false;
        }

        //2. Si chargement est OK, établir la connexion
        //----------------------------------------------
        if (estCharge) {
            //Récupération des paramètres de connexion
            String nom = lesParams.getNom();
            String mdp = lesParams.getMdp();
            String urlServeur = lesParams.getUrlServeur();

            //Création de la connexion
            try {
                con = (Connection) DriverManager.getConnection(urlServeur, nom, mdp);
                System.out.println("Connexion BD -> OK");
                return con;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Impossible de se connecter "
                        + "à la base de données " + e.getMessage(), "ALERTE",
                        JOptionPane.ERROR_MESSAGE);
                return null;
            }

        } else {
            return null;
        }
    }

    public static void fermetureConnexion()
    {
            try {
                con.close();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Problème à la fermeture de la connexion","ERREUR",JOptionPane.ERROR_MESSAGE);
            }

    }

    public static boolean controleSaisie(String nom, String mdp) {
        Parametres lesParams=new Parametres();

        if(nom.equals(lesParams.getNom()) && mdp.equals(lesParams.getMdp()) )
        {
            return true;
        }

	JOptionPane.showMessageDialog(null, "Erreur de saisie","ERREUR",JOptionPane.ERROR_MESSAGE);
        return false;
    }

    public static void testCon() throws SQLException {
        Connection cx = ConnexionDB.getConnexion();
        Statement etatSimple;
        ResultSet jeuEnreg;

        String nom;
        String client_id;
        String prenom;
        String commandeSQL = "select * from CLIENT;";
        //Créer un état
        etatSimple = cx.createStatement();
        //Exécuter la commande SQL et récupération du jeu d'enregistrements
        jeuEnreg = etatSimple.executeQuery(commandeSQL);

        System.out.println("CLIENT_ID  ,   NOM ,   PRENOM");
        //Parcourrir le jeu d'enregistrements
        while (jeuEnreg.next()) {
            client_id = jeuEnreg.getString("CLIENT_ID");
            nom = jeuEnreg.getString("NOM");
            prenom = jeuEnreg.getString("PRENOM");

            System.out.println(client_id + " , " + nom + " , " + prenom);

        }
        etatSimple.close();
     
    }   
}