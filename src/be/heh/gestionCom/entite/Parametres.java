package be.heh.gestionCom.entite;

public class Parametres
{
    private String nom;
    private String mdp;
    private String urlServeur;
    private String driver;

    public Parametres()
    {
        this.nom="root";
        this.mdp="";
        this.driver="org.gjt.mm.mysql.Driver";
        this.urlServeur="jdbc:mysql://localhost:3306/bd_site_e";
    }

    //Accesseurs
    public String getNom()
    {
        return this.nom;
    }

    public String getMdp()
    {
        return this.mdp;
    }

    public String getDriver()
    {
        return this.driver;
    }

    public String getUrlServeur()
    {
        return this.urlServeur;
    }

    //Mutateurs
    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public void setMdp(String mdp)
    {
        this.mdp = mdp;
    }

    public void setDriver(String driver)
    {
        this.driver = driver;
    }

    public void setUrlServeur(String urlServeur)
    {
        this.urlServeur = urlServeur;
    }
}
