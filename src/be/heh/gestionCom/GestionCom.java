/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package be.heh.gestionCom;

import be.heh.gestionCom.controle.connexion.ConnexionDB;
import be.heh.gestionCom.vue.FenConnexion;
import java.sql.Connection;
import javax.swing.JFrame;

/**
 *
 * @author Logan et kevin Detrain
 */
public class GestionCom extends JFrame
{

    public GestionCom()
    {

    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        Connection con = ConnexionDB.getConnexion();

        //Si la connexion ne peut pas être établie, alors le programme ne sert à rien. On le quitte.
        if(con == null)
            System.exit(0);

        FenConnexion fenCon = new FenConnexion();
        fenCon.setVisible(true);
    }
}
