/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package be.heh.gestionCom.vue;

import be.heh.gestionCom.controle.connexion.ConnexionDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Logan et kevin Detrain
 */
public class FenProduits extends JFrame
{
    private JTable table = null;
    private TableModel model = null;
    private JScrollPane scrollpane = null;
    private Vector<String> nomsColonnes = null;
    private Vector<Vector<String>> tabValeurs = null;

    public FenProduits()
    {
        Connection con = ConnexionDB.getConnexion();

        //On utilise des Vector pour rendre tout dynamique
        tabValeurs = new Vector<>();
        nomsColonnes = new Vector<>();

        try
        {
            Statement stat = con.createStatement();
            ResultSet resultat = stat.executeQuery("select * from produit");
            ResultSetMetaData meta = resultat.getMetaData();

            int nbrColonnes = meta.getColumnCount();

            //On récupere les noms des colonnes
            for(int i=1; i<=nbrColonnes; i++)
            {
                nomsColonnes.add(meta.getColumnName(i));
            }

            //On récupere les valeurs 
            for(int i=0; resultat.next(); i++)
            {
                tabValeurs.add(new Vector<String>()); //On ajoute une ligne à notre tableau

                for(int j=1; j<=nbrColonnes; j++)
                {
                    tabValeurs.get(i).add(resultat.getString(j)); //On ajoute les valeurs dans les colonnes de la ligne actuelle
                }
            }
        }
        catch (SQLException ex)
        {
        }

        model = new DefaultTableModel(tabValeurs, nomsColonnes);
        table = new JTable(model);
        scrollpane = new JScrollPane(table);
        this.getContentPane().add(scrollpane);

        this.setSize(640, 480);
        this.setVisible(true);
    }
}
